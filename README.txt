node {
  stage('SCM') {
    git 'https://gitlab.com/Neha2615/HappyTrips-JAVA.git'
  }
  stage('SonarQube analysis') {
    withSonarQubeEnv(credentialsId: ' d8a093e3afba7618938beee258cd18d4cdf799ed', installationName: 'My SonarQube Server') { // You can override the credential to be used
      sh 'mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.6.3:sonar'
    }
  }
}
